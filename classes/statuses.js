const Status = require('../models/status');
const { io } = require('../app');

class Statuses {

    constructor() {}

    find(name) {
        return Status.findOne({ name: name }).exec();
    }

}

module.exports = {
    Statuses
}