const Zone = require('../models/zone');
const { io } = require('../app');

class Zones {

    constructor() {}

    findByLocation(location) {
        return Zone.find({
            polygon: {
                $geoIntersects: {
                    $geometry: location
                }
            }
        }).exec();
    }

}

module.exports = {
    Zones
}