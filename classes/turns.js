const Turn = require('../models/turn');
const Driver = require('../models/driver');
const Subzone = require('../models/subzone');
const Zone = require('../models/zone');
const { io } = require('../app');
const _ = require("underscore");

const { Zones } = require('./zones');

const objZones = new Zones();


class Turns {

    createTurn(driver, subzone) {
        this.findTurnByDriver(driver)
            .then(
                (turnDB) => {
                    if (turnDB) {
                        if (turnDB.subzone._id.equals(driver.subzone._id)) {
                            return console.log('Misma subzona');
                        } else {
                            this.deleteTurn(driver);
                            this.insertTurn(driver, subzone);
                            return console.log('Cambio subzona');
                        }
                    }
                    this.insertTurn(driver, subzone);
                }
            )
            .catch(
                (err) => {
                    console.log(err);
                }
            );
    }

    insertTurn(driver, subzone) {
        let turn = new Turn({
            driver: driver._id,
            subzone: subzone._id
        });
        turn.save((err, turnDB) => {
            if (err) {
                return console.log(err);
            }
            this.refreshPosition();
            this.refreshListTurns(driver);
            return console.log('Turno creado');
        });
    }

    findTurnByDriver(driver) {
        return Turn.findOne({ driver: driver._id }).populate('subzone').exec();
    }

    findTurnBySubzone(subzone) {
        return Turn.findOne({ subzone: subzone._id })
            .sort({ created_at: 1 })
            .limit(1)
            .populate('driver').exec();
    }

    deleteTurn(driver) {
        Turn.findOneAndRemove({ driver: driver._id }, (err, turnDrop) => {
            if (err) {
                return console.log(err);
            }
            if (turnDrop) {
                this.refreshPosition();
                this.refreshListTurns(driver);
                return console.log('Turno eliminado');
            }
        });
    }

    refreshListTurns(driver) {
        objZones.findByLocation(driver.location)
            .then(
                zoneDB => {
                    if (zoneDB.length > 0)
                        this.getListTurns({ zoneId: zoneDB[0]._id });
                }
            )
            .catch(
                err => {
                    console.log(err);
                }
            );
    }

    refreshPosition() {
        Turn.find({}, 'subzone driver created_at updated_at')
            .sort({ created_at: 1 })
            .populate('subzone')
            .populate('driver')
            .exec((err, turns) => {
                if (err) {
                    return console.log(err);
                }
                let i = 0;
                turns.forEach(function(turn) {
                    i++;
                    turn.driver.position = i;

                    var body = _.pick(turn.driver, ['name', 'email', 'phone', 'numeral', 'img', 'fcmkey', 'status', 'subzone', 'position', 'location']);
                    Driver.findByIdAndUpdate(turn.driver._id, body, { new: true, runValidators: true, context: 'query' })
                        .populate('status', 'name')
                        .populate('subzone', 'name')
                        .populate('car')
                        .exec((err, driverDB) => {
                            if (err) {
                                console.log(err);
                                return;
                            }
                            io.to(driverDB.socketId).emit('updateDriver', driverDB);
                        });

                });
            });
    }

    listTurns(zoneId) {

        Subzone.aggregate([
            { $match: { zone: zoneId } },
            {
                $lookup: {
                    from: "turns",
                    localField: "_id",
                    foreignField: "subzone",
                    as: "turns"
                }
            }
        ]).exec(
            (err, subzonesDB) => {
                if (err) {
                    return;
                }

                turns = [];

                subzonesDB.forEach(function(subzone) {
                    turns.push({
                        subzone: subzone.name,
                        count: subzone.turns
                    });
                });

                io.emit('respListTurns', turns);

            }
        );

    }

    getListTurns(zoneId) {
        Zone.findOne({
            _id: zoneId.zoneId
        }).exec(
            (err, zoneDB) => {
                if (err) {
                    return;
                }
                Subzone.aggregate([{
                    $match: {
                        zone: zoneDB._id
                    }
                }, {
                    $lookup: {
                        from: "turns",
                        localField: "_id",
                        foreignField: "subzone",
                        as: "turns"
                    }
                }, {
                    $unwind: {
                        path: "$turns",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $sort: {
                        "turns.created_at": -1
                    }
                }, {
                    $lookup: {
                        from: "drivers",
                        localField: "turns.driver",
                        foreignField: "_id",
                        as: "turns.driver",
                    }
                }, {
                    $group: {
                        _id: "$_id",
                        name: { $first: "$name" },
                        turns: { $addToSet: "$turns" }
                    }
                }]).exec(
                    (err, subzonesDB) => {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        io.emit('respListTurns', subzonesDB);

                    }
                );
            }
        )
    }

}

module.exports = {
    Turns
}