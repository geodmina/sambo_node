const Rate = require('../models/rate');
const { io } = require('../app');

class Rates {

    constructor() {}

    findRate(zone) {

        const now = new Date();
        const dateSearch = new Date(2000, 0, 1, now.getHours(), now.getMinutes(), now.getSeconds());


        return Rate.findOne({
            $or: [{
                    $and: [{ time_start: { $lte: dateSearch } }, { time_end: { $gte: dateSearch } }]
                },
                {
                    $and: [{ time_start: { $lte: dateSearch } }, { $expr: { $gte: ["$time_start", "$time_end"] } }]
                },
                {
                    $and: [{ time_end: { $gte: dateSearch } }, { $expr: { $gte: ["$time_start", "$time_end"] } }]
                }
            ]
        }).exec();

    }

}

module.exports = {
    Rates
}