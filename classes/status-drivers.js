const StatusDriver = require('../models/status_driver');
const { io } = require('../app');

class StatusDrivers {

    constructor() {}

    find(name) {
        return StatusDriver.findOne({ name: name }).exec();
    }

}

module.exports = {
    StatusDrivers
}