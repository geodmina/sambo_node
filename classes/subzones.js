const Subzone = require('../models/subzone');
const { io } = require('../app');

class Subzones {

    constructor() {}

    findByLocation(location) {
        return Subzone.find({
            polygon: {
                $geoIntersects: {
                    $geometry: location
                }
            }
        }).exec();
    }

}

module.exports = {
    Subzones
}