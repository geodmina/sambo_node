const { io } = require('../app');
const Race = require('../models/race');
const _ = require('underscore');

const { Statuses } = require('./statuses');
const { Subzones } = require('./subzones');
const { Zones } = require('./zones');
const { Turns } = require('./turns');
const { Drivers } = require('./drivers');
const { StatusDrivers } = require('./status-drivers');
const { Rates } = require('./rates');

const objStatuses = new Statuses();
const objSubzones = new Subzones();
const objZones = new Zones();
const objTurn = new Turns();
const objDriver = new Drivers();
const objStatusDriver = new StatusDrivers();
const objRate = new Rates();

class Races {

    createRace(race, callback) {

        objZones.findByLocation(race.origin)
            .then(
                zoneDB => {
                    if (zoneDB.length < 1) {
                        return callback({
                            raceResp: {
                                ok: false,
                                errors: {
                                    message: 'La carrera esta fuera del área de cobertura'
                                }
                            }
                        });
                    } else {
                        return objRate.findRate(zoneDB);
                    }
                }
            )
            .then(
                rateDB => {
                    race.fare = rateDB;
                    return objSubzones.findByLocation(race.origin);
                }
            )
            .then(
                subzoneDB => {
                    if (race.fare) {
                        if (subzoneDB) {
                            if (subzoneDB.length < 1) {
                                console.log('La carrera no es de base');
                                this.asingNear(race, callback);
                            } else {
                                console.log(`La carrera es de la base ${ subzoneDB[0].name }`);
                                this.asingTurn(race, subzoneDB[0], callback);
                            }
                        }
                    }
                }
            )
            .catch(
                err => {
                    console.log(err);
                    callback({
                        raceResp: {
                            ok: false,
                            errors: err
                        }
                    });
                }
            );
    }

    asingTurn(race, subzoneDB, callback) {
        race.subzone = subzoneDB;
        objTurn.findTurnBySubzone(subzoneDB)
            .then(
                turnDB => {
                    if (!turnDB) {
                        // No hay turnos se va como ingresada
                        this.insertRace(race, 'INGRESADA', callback);
                    } else {
                        race.driver = turnDB.driver;
                        this.insertRace(race, 'ACEPTADA', callback);
                        // cambiarle el estado y notificarle de la carrera
                    }
                }
            )
            .catch(
                err => {
                    console.log(err);
                    callback({
                        raceResp: {
                            ok: false,
                            errors: err
                        }
                    });
                }
            );
    }

    asingNear(race, callback) {
        objStatusDriver.find('EN LINEA')
            .then(
                statusDriverDB => {
                    return objDriver.findDriverNear(race.origin, statusDriverDB);
                }
            )
            .then(
                driverDB => {
                    if (driverDB.length > 0) {
                        // cambiarle el estado y notificarle de la carrera
                        race.driver = driverDB[0];
                        this.insertRace(race, 'ACEPTADA', callback);
                    } else {
                        // No hay mas cercano se va como ingresada
                        this.insertRace(race, 'INGRESADA', callback);
                    }
                }
            )
            .catch(
                err => {
                    console.log(err);
                    callback({
                        raceResp: {
                            ok: false,
                            errors: err
                        }
                    });
                }
            );
    }

    insertRace(race, status, callback) {
        objStatuses.find(status)
            .then(
                statusDB => {
                    race.status = statusDB;
                    let objRace = new Race(race);
                    return objRace.save();
                }
            )
            .then(
                doc => {

                    this.findRaceById(doc._id)
                        .then(
                            raceDB => {
                                this.notifyRace(raceDB, status);
                            }
                        )
                        .catch(
                            err => {
                                console.log(err);
                            }
                        );

                    this.getRacesDispatcher();

                    callback({
                        raceResp: {
                            ok: true,
                            race: doc
                        }
                    });
                }
            )
            .catch(
                err => {
                    console.log(err);
                    callback({
                        raceResp: {
                            ok: false,
                            errors: err
                        }
                    });
                }
            );
    }

    findRaceById(raceId) {
        return Race.findById(raceId)
            .populate('passenger')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .populate('status')
            .populate('subzone')
            .populate('fare')
            .exec();
    }

    updateRace(race) {
        var body = _.pick(race, ['status', 'address_arrival', 'destination', 'driver', 'cost', 'distance', 'time_wait', 'time_race', 'start_race', 'end_race', 'recalled', 'rate', 'comment']);
        return Race.findByIdAndUpdate(race._id, body, { new: true, runValidators: true, context: 'query' })
            .populate('passenger', 'name phone email img')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .populate('status', 'name')
            .populate('subzone', 'name')
            .populate('fare')
            .exec();
    }

    notifyRace(race, status) {
        if (status === 'ACEPTADA') {
            objStatusDriver.find('EN CAMINO')
                .then(
                    statusDriverDB => {
                        race.driver.subzone = null;
                        race.driver.position = 0;
                        race.driver.status = statusDriverDB;
                        objTurn.deleteTurn(race.driver);
                        return objDriver.updateDriver(race.driver);
                    }
                )
                .then(
                    driverDB => {
                        console.log(race);
                        io.to(driverDB.socketId).emit('notifyRace', race);
                    }
                )
                .catch(
                    err => {
                        console.log(err);
                    }
                );
        }
    }

    changeStatus(data) {

        objStatuses.find(data.status)
            .then(
                statusDB => {
                    data.race.status = statusDB;
                    if (statusDB.name === 'CANCELADA CONDUCTOR') {
                        data.race.recalled++;
                    }
                    return this.updateRace(data.race);
                }
            )
            .then(
                raceDB => {
                    switch (data.status) {
                        case 'ABORDADA':
                            return objStatusDriver.find('EN CARRERA');
                        case 'FINALIZADA':
                            return objStatusDriver.find('EN LINEA');
                        case 'CANCELADA CONDUCTOR':
                            return objStatusDriver.find('EN LINEA');
                        case 'CANCELADA USUARIO':
                            return objStatusDriver.find('EN LINEA');
                        case 'INGRESADA':
                            return objStatusDriver.find('EN LINEA');
                        default:
                            break;
                    }
                }
            )
            .then(
                statusDriverDB => {
                    data.race.driver.status = statusDriverDB;
                    return objDriver.updateDriver(data.race.driver)
                }
            )
            .then(
                driverDB => {
                    switch (data.status) {
                        case 'ABORDADA':
                            io.to(driverDB.socketId).emit('repstartRace', {
                                ok: true,
                                race: data.race
                            });
                            break;
                        case 'FINALIZADA':
                            io.to(driverDB.socketId).emit('repEndRace', {
                                ok: true,
                                driver: driverDB
                            });
                            break;
                        case 'CANCELADA USUARIO':
                            io.to(driverDB.socketId).emit('cancelUser', {
                                ok: true,
                                driver: driverDB
                            });
                            break;
                        case 'CANCELADA CONDUCTOR':
                            io.to(driverDB.socketId).emit('repCancelDriver', {
                                ok: true,
                                driver: driverDB
                            });
                            if (data.race.recalled < 3) {
                                this.asingDriver(data.race);
                            } else {
                                this.noResponseRace(data.race);
                            }

                            break;
                        default:
                            break;
                    }
                    if (data.race.central)
                        this.getRacesDispatcher();
                }
            )
            .catch(
                err => {
                    console.log(err);
                    let resp = {
                        ok: false
                    };
                    switch (data.status) {
                        case 'ABORDADA':
                            io.to(data.race.driver.socketId).emit('repstartRace', resp);
                            break;
                        case 'FINALIZADA':
                            io.to(data.race.driver.socketId).emit('repEndRace', resp);
                            break;
                        case 'CANCELADA CONDUCTOR':
                            io.to(driver.socketId).emit('repCancelDriver', resp);
                            break;
                        default:
                            break;
                    }
                }
            )
    }

    noResponseRace(race) {
        objStatuses.find('SIN RESPUESTA')
            .then(
                statusDB => {
                    race.driver = null;
                    race.status = statusDB;
                    return this.updateRace(race);
                }
            )
            .then(
                raceDB => {
                    console.log('Guardada SIN RESPUESTA');
                    io.emit('notifyTimeRace', race);
                    this.getRacesDispatcher();
                }
            )
            .catch(
                err => {
                    console.log(err);
                }
            );
    }

    asingDriver(race) {
        if (race.subzone) {
            objStatuses.find('ACEPTADA')
                .then(
                    statusDB => {
                        race.status = statusDB;
                        return objTurn.findTurnBySubzone(race.subzone);
                    }
                )
                .then(
                    turnDB => {
                        if (!turnDB) {
                            race.driver = null;
                            this.changeStatus({
                                status: 'INGRESADA',
                                race: race
                            });
                        } else {
                            race.driver = turnDB.driver;
                            this.updateRace(race)
                                .then(
                                    raceDB => {
                                        this.getRacesDispatcher();
                                        this.notifyRace(raceDB, 'ACEPTADA')
                                    }
                                )
                        }
                    }
                )
                .catch(
                    err => {
                        console.log(err);
                    }
                );
        } else {
            objStatuses.find('ACEPTADA')
                .then(
                    statusDB => {
                        race.status = statusDB;
                        return objStatusDriver.find('EN LINEA');
                    }
                )
                .then(
                    statusDriverDB => {
                        return objDriver.findDriverNear(race.origin, statusDriverDB);
                    }
                )
                .then(
                    driverDB => {
                        console.log(driverDB);
                        console.log(race.recalled);
                        console.log(driverDB[race.recalled]);
                        if (driverDB.length > 0) {
                            if (driverDB[race.recalled]) {
                                race.driver = driverDB[race.recalled];
                                this.updateRace(race)
                                    .then(
                                        raceDB => {
                                            this.getRacesDispatcher();
                                            this.notifyRace(raceDB, 'ACEPTADA')
                                        }
                                    )
                            } else {
                                race.driver = driverDB[0];
                                this.updateRace(race)
                                    .then(
                                        raceDB => {
                                            this.getRacesDispatcher();
                                            this.notifyRace(raceDB, 'ACEPTADA')
                                        }
                                    )
                            }
                        } else {
                            race.driver = null;
                            this.changeStatus({
                                status: 'INGRESADA',
                                race: race
                            });
                        }
                    }
                )
                .catch(
                    err => {
                        console.log(err);
                    }
                );
        }
        this.getRacesDispatcher();
    }

    findWithoutResponse(status, date) {

        return Race.find({ status: status, created_at: { $lte: date } })
            .sort({ created_at: -1 })
            .populate('passenger', 'name phone')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .populate('status', 'name')
            .populate('fare')
            .exec();
    }

    getRacesDispatcher() {
        var d = new Date();
        d.setMinutes(d.getMinutes() - 120);
        Race.find({ created_at: { $gte: d } })
            .sort({ created_at: -1 })
            .populate('passenger', 'name phone')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate('status', 'name')
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .exec(((err, listRaces) => {
                if (err) {
                    return;
                }
                io.emit('listRaces', listRaces);
            }));
    }

    acceptRace(data) {

        Race.findById(data.race._id)
            .exec(
                (err, raceDB) => {
                    if (err) {
                        io.to(data.race.driver.socketId).emit('respAcceptRace', {
                            ok: false,
                            message: 'Error al aceptar carrera'
                        });
                    }

                    if (raceDB.driver) {
                        io.to(data.race.driver.socketId).emit('respAcceptRace', {
                            ok: false,
                            message: 'Carrera no se encuentra disponible'
                        });
                    }

                    objStatusDriver.find("EN CAMINO")
                        .then(
                            statusDriverDB => {
                                data.race.driver.status = statusDriverDB;
                                objDriver.updateDriver(data.race.driver);
                                objTurn.deleteTurn(data.race.driver);
                                return objStatuses.find(data.status);
                            }
                        )
                        .then(
                            statusDB => {
                                data.race.status = statusDB
                                return this.updateRace(data.race)
                            }
                        )
                        .then(
                            raceDB => {
                                io.to(data.race.driver.socketId).emit('respAcceptRace', {
                                    ok: true,
                                    message: 'Carrera Aceptada',
                                    race: raceDB
                                });
                            }
                        )
                        .catch(
                            err => {
                                io.to(data.race.driver.socketId).emit('respAcceptRace', {
                                    ok: false,
                                    message: 'Error al aceptar carrera'
                                });
                            }
                        )

                }
            )

    }

}

module.exports = {
    Races
}