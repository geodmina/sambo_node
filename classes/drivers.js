const Driver = require('../models/driver');
const Race = require('../models/race');
const Status = require('../models/status');
const StatusDriver = require('../models/status_driver');

const _ = require('underscore');
const { io } = require('../app');

const { Turns } = require('./turns');
const { Subzones } = require('./subzones');
const { StatusDrivers } = require('./status-drivers');

const objTurn = new Turns();
const objStatusDriver = new StatusDrivers();
const objSubzones = new Subzones();

class Drivers {

    constructor() {
        this.drivers = [];
    }

    getDriver(id) {
        let driver = this.drivers.filter(driver => driver.id === id)[0];
        return driver;
    }

    addDriver(id, driverId) {
        let driver = { id, driverId };
        this.drivers.push(driver);

        this.findDriverById(driverId)
            .then(
                driverDB => {
                    if (driverDB) {
                        driverDB.socketId = id;
                        return this.updateDriver(driverDB);
                    }
                })
            .then(
                driver => {
                    console.log(`Usuario conectado ${ driver.name }`);
                    io.to(driver.socketId).emit('registerDriver', driver);
                }
            )
            .catch(
                err => {
                    console.log(err);
                }
            );

        return driver;
    }

    validRegister(id, driverId) {
        let driver = this.drivers.filter(driver => driver.driverId === driverId)[0];
        if (!driver) {
            this.addDriver(id, driverId);
        }
    }
    removeDriver(id) {
        let driverRemove = this.getDriver(id);
        this.drivers = this.drivers.filter(driver => driver.id != id);
        if (driverRemove) {

            this.findDriverById(driverRemove.driverId)
                .then(
                    driverDB => {
                        if (driverDB) {
                            if (driverDB.status.name === 'EN LINEA') {
                                objStatusDriver.find('DESCONECTADO')
                                    .then(
                                        statusDriverDB => {
                                            driverDB.status = statusDriverDB;
                                            driverDB.subzone = null;
                                            driverDB.position = 0;
                                            objTurn.deleteTurn(driverDB);
                                            this.updateDriver(driverDB).then(
                                                driver => {
                                                    console.log(`Usuario desconectado ${ driver.name }`);
                                                }
                                            );
                                        }
                                    )
                            } else {
                                this.updateDriver(driverDB)
                                    .then(
                                        driver => {
                                            console.log(`Usuario desconectado ${ driver.name }`);
                                        }
                                    )
                            }
                        }

                    }
                )
                .catch(
                    (err) => {
                        console.log(err);
                    }
                );
        }
        return driverRemove;
    }

    updateDriverS(data, id) {
        data.driver.socketId = id;
        this.validRegister(id, data.driver._id);
        switch (data.type) {
            case 'location':
                this.findSubzone(data.driver);
                break;
            case 'status':
                this.updateStatus(data.driver, data.status);
                break;
            default:
                break;
        }
    }

    findSubzone(driver) {
        console.log('=========================');
        console.log(driver.status.name);
        console.log('=========================');

        switch (driver.status.name) {
            case 'EN LINEA':
                objSubzones.findByLocation(driver.location)
                    .then(
                        (subzoneDB) => {
                            if (subzoneDB.length < 1) {
                                this.deleteSubzoneOfDriver(driver);
                                this.findRaceNear(driver);
                                return console.log(`Driver ${ driver.name } no se ecuentra en zona actualizado`);
                            }
                            driver.subzone = subzoneDB[0];
                            objTurn.createTurn(driver, subzoneDB[0]);
                            this.updateDriver(driver)
                                .then(
                                    (driverDB) => {
                                        console.log(`Driver ${ driver.name } en la zona ${ subzoneDB[0].name } actualizado`);
                                        this.findRaceBySubzone(driver, subzoneDB);
                                    }
                                )
                                .catch(
                                    (err) => {
                                        console.log(err);
                                    }
                                );
                        }
                    )
                    .catch(
                        (err) => {
                            console.log(err);
                        }
                    );
                break;
            case 'DESCONECTADO':
                this.deleteSubzoneOfDriver(driver);
                break;
            case 'OCUPADO':
                this.deleteSubzoneOfDriver(driver);
                break;
            case 'EN CAMINO':
                console.log('Tiene carrera ACEPTADA hay que enviarle');
                this.findRaceProcess(driver, 'ACEPTADA');
                break;
            case 'EN CARRERA':
                console.log('Tiene carrera ABORDADA hay que enviarle');
                this.findRaceProcess(driver, 'ABORDADA');
                break;
            default:
                break;
        }
    }

    deleteSubzoneOfDriver(driver) {
        driver.subzone = null;
        driver.position = 0;
        objTurn.deleteTurn(driver);
        this.updateDriver(driver)
            .then(
                (driverDB) => {
                    io.to(driverDB.socketId).emit('updateDriver', driverDB);
                }
            )
            .catch(
                (err) => {
                    console.log(err);
                }
            );
    }

    findRaceProcess(driver, status) {

        this.updateDriver(driver)
            .then(
                (driverDB) => {
                    console.log(`Driver ${ driver.name } actualizado`);
                    io.to(driverDB.socketId).emit('updateDriver', driverDB);
                }
            )
            .catch(
                (err) => {
                    console.log(err);
                }
            );

        Status.findOne({ name: status }, (err, statusDB) => {
            if (err)
                return console.log('Error al consultar estado de la carrera')
            Race.findOne({ status: statusDB._id, driver: driver._id })
                .populate('passenger')
                .populate({
                    path: 'driver',
                    populate: { path: 'status' }
                })
                .populate({
                    path: 'driver',
                    populate: { path: 'car' }
                })
                .populate('status')
                .populate('subzone')
                .populate('fare')
                .exec((err, race) => {

                    if (err)
                        return console.log('Error al consultar carreras');

                    if (race)
                        io.to(driver.socketId).emit('notifyRace', race);

                });
        });
    }

    findRaceNear(driver) {
        Status.findOne({ name: 'INGRESADA' }, (err, statusDB) => {
            if (err)
                return console.log('Error al consultar estado de la carrera')

            Race.find({
                status: statusDB,
                origin: {
                    $near: {
                        $geometry: driver.location,
                        $maxDistance: 1937
                    }
                }
            }).exec(
                (err, raceDB) => {

                    if (err)
                        return console.log('Error al consultar carrera')

                    if (raceDB.length > 0) {
                        this.asingRace(driver, raceDB[0]);
                    }

                });
        });
    }

    findRaceBySubzone(driver, subzone) {
        Status.findOne({ name: 'INGRESADA' }, (err, statusDB) => {
            if (err)
                return console.log('Error al consultar estado de la carrera')

            Race.findOne({ subzone: subzone, status: statusDB }, (err, raceDB) => {
                if (err)
                    return console.log('Error al consultar carrera')

                if (raceDB) {
                    this.asingRace(driver, raceDB);
                } else {
                    this.findRaceNear(driver);
                }

            })
        });

    }

    asingRace(driver, race) {
        objStatusDriver.find('EN CAMINO')
            .then(
                statusDriverDB => {
                    driver.status = statusDriverDB;
                    driver.subzone = null;
                    driver.position = 0;
                    objTurn.deleteTurn(driver);
                    return this.updateDriver(driver);
                }
            )
            .then(
                driverDB => {
                    Status.findOne({ name: 'ACEPTADA' }, (err, statusUpdate) => {
                        if (err)
                            return console.log('Error al consultar estado de la carrera')

                        race.driver = driverDB;
                        race.status = statusUpdate;
                        var body = _.pick(race, ['status', 'address_arrival', 'destination', 'driver', 'cost', 'distance', 'time_wait', 'time_race', 'start_race', 'end_race', 'recalled', 'rate', 'comment']);
                        Race.findByIdAndUpdate(race._id, body, { new: true, runValidators: true, context: 'query' })
                            .populate('passenger', 'name phone email img')
                            .populate({
                                path: 'driver',
                                populate: { path: 'status' }
                            })
                            .populate({
                                path: 'driver',
                                populate: { path: 'car' }
                            })
                            .populate('status', 'name')
                            .populate('subzone', 'name')
                            .populate('fare')
                            .exec(
                                (err, raceDB) => {
                                    if (err)
                                        return console.log('Error al consultar carrera');

                                    if (raceDB) {
                                        this.getRacesDispatcher();
                                        io.to(driverDB.socketId).emit('notifyRace', raceDB);
                                    }

                                }
                            );

                    });
                }
            )
            .catch(
                err => {
                    console.log(err);
                }
            );
    }

    updateStatus(driver, status) {
        objStatusDriver.find(status)
            .then(
                statusDriverDB => {
                    driver.status = statusDriverDB;
                    return this.updateDriver(driver);
                }
            )
            .then(
                driverDB => {
                    this.findSubzone(driverDB);
                    //io.to(driverDB.socketId).emit('updateDriver', driverDB);
                    console.log('Estado actualizado');
                }
            )
            .catch(
                err => {
                    console.log(err);
                }
            );
    }

    findDriverById(driverID) {
        return Driver.findById(driverID).populate('status', 'name').populate('subzone', 'name').populate('car').exec();
    }

    findDriverNear(location, status) {
        return Driver.find({
            status: status,
            location: {
                $near: {
                    $geometry: location,
                    $maxDistance: 1937
                }
            }
        }).exec();
    }

    updateDriver(driver) {
        var body = _.pick(driver, ['name', 'email', 'phone', 'numeral', 'img', 'fcmkey', 'status', 'subzone', 'position', 'location', 'socketId']);
        return Driver.findByIdAndUpdate(driver._id, body, { new: true, runValidators: true, context: 'query' })
            .populate('status', 'name')
            .populate('subzone', 'name')
            .populate('car')
            .exec();
    }

    suspendDriver(status, driver, callback) {
        objStatusDriver.find(status)
            .then(
                statusDriverDB => {
                    driver.status = statusDriverDB;
                    return this.updateDriver(driver);
                }
            )
            .then(
                driverDB => {
                    let message = '';
                    console.log('====> ' + status);
                    if (status === 'SUSPENDIDO') {
                        io.to(driverDB.socketId).emit('suspendAccount', driverDB);
                        message = `Conductor # ${ driverDB.numeral } - ${driverDB.name} bloqueado`;
                    } else {
                        message = `Conductor # ${ driverDB.numeral } - ${driverDB.name} desbloqueado`;
                    }

                    callback({
                        response: {
                            ok: true,
                            message: message
                        }
                    });
                }
            )
            .catch(
                err => {
                    console.log(err);

                    let message = '';
                    if (status === 'SUSPENDIDO') {
                        io.to(driverDB.socketId).emit('suspendAccount', driverDB);
                        message = 'Error al bloquear al conductor';
                    } else {
                        message = 'Error al desbloquear al conductor';
                    }

                    callback({
                        response: {
                            ok: false,
                            message: message,
                            errors: err
                        }
                    });
                }
            )
    }

    getRacesDispatcher() {
        var d = new Date();
        d.setMinutes(d.getMinutes() - 120);
        Race.find({ created_at: { $gte: d } })
            .sort({ created_at: -1 })
            .populate('passenger', 'name phone')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate('status', 'name')
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .exec(((err, listRaces) => {
                if (err) {
                    return;
                }
                io.emit('listRaces', listRaces);
            }));
    }

}

module.exports = {
    Drivers
}