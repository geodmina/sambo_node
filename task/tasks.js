const { Statuses } = require('../classes/statuses');
const { Races } = require('../classes/races');
const { io } = require('../app');

const schedule = require('node-schedule');

const objStatuses = new Statuses();
const objRaces = new Races();


var j = schedule.scheduleJob('*/3 * * * *', function() {

    findRacesWithoutresponse();

});

function findRacesWithoutresponse() {

    objStatuses.find('INGRESADA')
        .then(
            statusDB => {
                let date = new Date();
                date.setMinutes(date.getMinutes() - 15);
                return objRaces.findWithoutResponse(statusDB, date)
            }
        )
        .then(
            racesDB => {
                if (racesDB.length < 1)
                    return console.log('No hay carreras');

                racesDB.forEach((race) => {
                    updateStatusRace(race);
                });

            }
        )
        .catch(
            err => {
                console.log(err);
            }
        );

}

function updateStatusRace(race) {

    objStatuses.find('SIN RESPUESTA')
        .then(
            statusDB => {
                race.status = statusDB;
                return objRaces.updateRace(race);
            }
        )
        .then(
            raceDB => {
                io.emit('notifyTimeRace', raceDB);
                console.log(`Cambiada de estado ${raceDB._id} : ${ raceDB.address_origin }`);
                objRaces.getRacesDispatcher();
            }
        )
        .catch(
            err => {
                console.log(err);
            }
        )

}