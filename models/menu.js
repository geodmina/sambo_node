const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);

let Schema = mongoose.Schema;

let menuSchema = new Schema({
    role: {
        type: String
    },
    header_menu: [{
        title: String,
        icon: String,
        url: String
    }],
    sidebar_menu: [{
        title: String,
        icon: String,
        submenu: [{
            title: String,
            url: String,
        }]
    }]
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

menuSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Menu', menuSchema)