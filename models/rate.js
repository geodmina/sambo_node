const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);
const SchemaTypes = mongoose.Schema.Types;

let Schema = mongoose.Schema;

let rateSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El campo nombre es requerido"]
    },
    zone: {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
    },
    value_start: {
        default: 0,
        type: SchemaTypes.Double
    },
    cost_min: {
        default: 0,
        type: SchemaTypes.Double
    },
    time_value: {
        default: 0,
        type: SchemaTypes.Double
    },
    distance_value: {
        default: 0,
        type: SchemaTypes.Double
    },
    time_start: {
        required: true,
        type: Date
    },
    time_end: {
        required: true,
        type: Date
    },
    time_wait: {
        default: 0,
        type: SchemaTypes.Double
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

rateSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Rate', rateSchema)