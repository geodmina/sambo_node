const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let statusDriverSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El camppo nombre es requerido"]
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

statusDriverSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('StatusDriver', statusDriverSchema)