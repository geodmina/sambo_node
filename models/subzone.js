const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);

let Schema = mongoose.Schema;

let subzoneSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El campo nombre es requerido"]
    },
    coordinate: {
        type: String,
        required: [true, "El campo coordenadas es requerido"]
    },
    polygon: {
        type: {
            type: String,
            default: "Polygon"
        },
        coordinates: [
            [
                [Number]
            ]
        ]
    },
    zone: {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

subzoneSchema.index({ polygon: "2dsphere" });

subzoneSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Subzone', subzoneSchema)