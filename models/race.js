var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);
const SchemaTypes = mongoose.Schema.Types;

var typeValid = {
    values: ['CORPORATIVE', 'CASH'],
    message: '{VALUE} is not a type valid'
};

var Schema = mongoose.Schema;

var raceSchema = new Schema({
    address_origin: {
        type: String,
        required: [true, "El campo dirección es requerido"]
    },
    origin: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: []
    },
    reference: {
        type: String,
        required: [true, "El campo referencia es requerido"]
    },
    passenger: {
        type: Schema.Types.ObjectId,
        ref: 'Passenger',
        required: [true, "El campo usuario es requerido"]
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'Status'
    },
    address_arrival: {
        type: String,
        default: null
    },
    destination: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: {
            type: [],
            default: [0, 0]
        }
    },
    subzone: {
        type: Schema.Types.ObjectId,
        ref: 'Subzone',
        default: null
    },
    driver: {
        type: Schema.Types.ObjectId,
        ref: 'Driver',
        default: null
    },
    cost: {
        type: SchemaTypes.Double,
        default: 0
    },
    distance: {
        type: SchemaTypes.Double,
        default: 0
    },
    time_wait: {
        type: String,
        default: 0
    },
    time_race: {
        type: String,
        default: 0
    },
    start_race: {
        type: String,
        default: 0
    },
    end_race: {
        type: String,
        default: 0
    },
    recalled: {
        type: Number,
        default: 0
    },
    rate: {
        type: SchemaTypes.Double,
        default: 0
    },
    comment: {
        type: String,
        default: null
    },
    central: {
        type: Boolean,
        default: false
    },
    payment: {
        type: String,
        default: 'CASH',
        enum: typeValid
    },
    fare: {
        type: Schema.Types.ObjectId,
        ref: 'Rate',
        default: null
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

raceSchema.index({ origin: "2dsphere" });

raceSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' });

module.exports = mongoose.model('Race', raceSchema);