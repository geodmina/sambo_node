const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let carSchema = new Schema({
    brand: {
        type: String,
        required: [true, "El campo marca es requerido"]
    },
    model: {
        type: String,
        required: [true, "El campo modelo es requerido"]
    },
    color: {
        type: String,
        required: [true, "El campo color es requerido"]
    },
    plaque: {
        type: String,
        required: [true, "El campo placa es requerido"]
    },
    driver: {
        type: Schema.Types.ObjectId,
        ref: 'Driver',
        required: [true, "El campo conductor es requerido"]
    },
    img: {
        type: String,
        required: false
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

carSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Car', carSchema);