const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);

let Schema = mongoose.Schema;

let turnSchema = new Schema({
    subzone: {
        type: Schema.Types.ObjectId,
        ref: 'Subzone',
        required: [true, "El campo base es requerido"]
    },
    driver: {
        type: Schema.Types.ObjectId,
        ref: 'Driver',
        unique: true,
        required: [true, "El campo conductor es requerido"]
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

turnSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Turn', turnSchema)