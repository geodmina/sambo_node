const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);

let Schema = mongoose.Schema;

let callSchema = new Schema({
    extension: {
        type: String,
        required: [true, "El campo extension es requerido"]
    },
    phone: {
        type: String,
        required: [true, "El campo telefono es requerido"]
    },
    operator: {
        type: Schema.Types.ObjectId,
        ref: 'Admin'
    },
    attended_at: {
        type: Date
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

callSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Call', callSchema)