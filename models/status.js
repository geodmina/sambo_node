const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let statusSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El campo nombre es requerido"]
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

statusSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Status', statusSchema)