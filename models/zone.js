const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);
const SchemaTypes = mongoose.Schema.Types;

let typeValue = {
    values: ['VALOR', 'PROCENTAJE'],
    message: '{VALUE} is not a type valid'
}

let Schema = mongoose.Schema;

let zoneSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El campo nombre es requerido"]
    },
    coordinate: {
        type: String,
        required: [true, "El campo coordenadas es requerido"]
    },
    polygon: {
        type: {
            type: String,
            default: "Polygon"
        },
        coordinates: [
            [
                [Number]
            ]
        ]
    },
    type_value: {
        type: String,
        default: 'VALOR',
        enum: typeValue
    },
    service_value: {
        default: 0,
        type: SchemaTypes.Double
    },
    status: {
        type: Boolean,
        default: true
    },
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

zoneSchema.index({ polygon: "2dsphere" });

zoneSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Zone', zoneSchema)