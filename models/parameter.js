const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);
const SchemaTypes = mongoose.Schema.Types;

let Schema = mongoose.Schema;

let parameterSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "El campo nombre es requerido"]
    },
    code: {
        type: String,
        unique: true,
        required: [true, "El campo codigo es requerido"]
    },
    text_value: {
        default: null,
        type: String
    },
    numeric_value: {
        default: 0,
        type: SchemaTypes.Double
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

parameterSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Parameter', parameterSchema)