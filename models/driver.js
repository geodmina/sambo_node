const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let driverSchema = new Schema({
    name: {
        type: String,
        required: [true, "El campo nombre es requerido"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El campo email es requerido"]
    },
    phone: {
        type: String,
        required: [true, "El campo telefono es requerido"]
    },
    numeral: {
        type: Number,
        default: 0
    },
    img: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: [true, "El campo contraseña es requerido"]
    },
    imei: {
        type: String,
        required: [true, "El campo imei es requerido"]
    },
    location: {
        type: {
            type: String,
            default: "Point"
        },
        coordinates: []
    },
    fcmkey: {
        type: String,
        required: false
    },
    subzone: {
        type: Schema.Types.ObjectId,
        ref: 'Subzone',
        default: null
    },
    position: {
        type: Number,
        default: 0
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'StatusDriver'
    },
    socketId: {
        type: String,
        default: null
    },
    car: {
        type: Schema.Types.ObjectId,
        ref: 'Car',
        default: null
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

driverSchema.index({ location: "2dsphere" });

driverSchema.methods.toJSON = function() {
    let driver = this;
    let driverObject = driver.toObject();
    delete driverObject.password;
    return driverObject;
}

driverSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Driver', driverSchema);