const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);
const SchemaTypes = mongoose.Schema.Types;

let Schema = mongoose.Schema;

let directionSchema = new Schema({
    address: {
        type: String,
        unique: true,
        required: [true, "El campo dirección es requerido"]
    },
    latitude: {
        type: SchemaTypes.Double,
        required: [true, "El campo latitud es requerido"]
    },
    longitude: {
        type: SchemaTypes.Double,
        required: [true, "El campo longitud es requerido"]
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

directionSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Direction', directionSchema)