var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var roleValid = {
    values: ['ADMIN_ROLE', 'OPERADOR_ROLE'],
    message: '{VALUE} no es un rol valido'
};

var Schema = mongoose.Schema;

var adminSchema = new Schema({
    name: {
        type: String,
        required: [true, "El campo nombre es requerido"]
    },
    username: {
        type: String,
        unique: true,
        required: [true, "El campo usuario es requerido"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El campo email es requerido"]
    },
    password: {
        type: String,
        required: [true, "El campo contraseña es requerido"]
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'OPERADOR_ROLE',
        enum: roleValid
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

adminSchema.methods.toJSON = function() {
    var admin = this;
    var adminObject = admin.toObject();
    delete adminObject.password;
    return adminObject;
};

adminSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

module.exports = mongoose.model('Admin', adminSchema);