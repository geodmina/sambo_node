var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var typeValid = {
    values: ['CORPORATIVE_ROLE', 'USER_ROLE'],
    message: '{VALUE} is not a type valid'
};

var Schema = mongoose.Schema;

var passengerSchema = new Schema({
    name: {
        type: String,
        required: [true, "El campo nombre es requerido"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El campo email es requerido"]
    },
    phone: {
        type: String,
        required: [true, "El campo teléfono es requerido"]
    },
    img: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: [true, "El campo contraseña es requerido"]
    },
    fcmkey: {
        type: String,
        required: false
    },
    type: {
        type: String,
        default: 'USER_ROLE',
        enum: typeValid
    },
    administrator: {
        type: Boolean,
        default: false
    },
    parent_id: {
        type: Schema.Types.ObjectId,
        ref: 'Passenger'
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

passengerSchema.methods.toJSON = function() {
    var passenger = this;
    var passengerObject = passenger.toObject();
    delete passengerObject.password;
    return passengerObject;
};

passengerSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' });

module.exports = mongoose.model('Passenger', passengerSchema);