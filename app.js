// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Iniciarlizar variables
var app = express();

var server = require('http').createServer(app);

app.use(express.static('public'));

var io = require('socket.io')(server);
module.exports.io = require('socket.io')(server);

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");

    next();
});

// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Conexión a la Base de Datos
mongoose.connection.openUri('mongodb://localhost:27017/samboroncarDB', { useNewUrlParser: true }, (err, res) => {
    if (err) throw err;
    console.log('DB OK');
});
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

// Rutas
app.use(require('./routes/index'));

// Socket
require('./sockets/socket');

// Cron
require('./task/tasks');

// Escuchar peticiones
server.listen(3000, () => {
    console.log('App Samboroncar escuchando puerto 3000');
});