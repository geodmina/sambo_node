const { io } = require('../app');
const Call = require('../models/call');
const Status = require('../models/status');
const Race = require('../models/race');
const Driver = require('../models/driver');
const Subzone = require('../models/subzone');
const Zone = require('../models/zone');

const { Drivers } = require('../classes/drivers');
const { Races } = require('../classes/races');

const drivers = new Drivers();
const races = new Races();

io.on('connection', (client) => {

    console.log('Usuario conectado => ' + client.id);

    client.on('disconnect', () => {
        console.log('Usuario desconectado => ' + client.id);
        drivers.removeDriver(client.id);
    });

    getCallDispatcher(true);
    getRacesDispatcher(true);

    // Driver
    client.on('registerDriver', function(driverId) {
        drivers.addDriver(client.id, driverId, client);
    });

    client.on('updateDriver', (data) => {
        let objJson = JSON.parse(data);
        drivers.updateDriverS(objJson, client.id);
    });

    client.on('suspendDriver', (driver, callback) => {
        drivers.suspendDriver('SUSPENDIDO', driver, callback);
    });

    client.on('activeDriver', (driver, callback) => {
        drivers.suspendDriver('EN LINEA', driver, callback);
    });

    // Carrera
    client.on('createRace', (data, callback) => {
        races.createRace(data, callback);
    });

    client.on('CancelDriver', (data) => {
        let objJson = JSON.parse(data);
        races.changeStatus(objJson);
    });

    client.on('startRace', (data) => {
        let objJson = JSON.parse(data);
        races.changeStatus(objJson);
    });

    client.on('endRace', (data) => {
        let objJson = JSON.parse(data);
        races.changeStatus(objJson);
    });

    client.on('cancelUser', (data) => {
        races.changeStatus(data);
    });

    client.on('acceptRace', (data) => {
        let objJson = JSON.parse(data);
        races.acceptRace(objJson);
    });

    // Lista de turnos

    client.on('lisTurns', (data) => {
        getListTurns(data);
    });

    function getCallDispatcher(onInit) {
        var d = new Date();
        d.setMinutes(d.getMinutes() - 15);
        Call.find({ created_at: { $gte: d } }, 'extension phone operator attended_at created_at updated_at')
            .sort({ created_at: -1 })
            .populate('operator', 'name')
            .exec(((err, calls) => {
                if (err) {
                    return;
                }
                if (onInit) {
                    client.emit('listCalls', calls);
                } else {
                    client.broadcast.emit('listCalls', calls);
                }
            }));
    }

    function getRacesDispatcher(onInit) {
        var d = new Date();
        d.setMinutes(d.getMinutes() - 120);
        Race.find({ created_at: { $gte: d } })
            .sort({ created_at: -1 })
            .populate('passenger', 'name phone')
            .populate('driver', 'name numeral')
            .populate('status', 'name')
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .exec(((err, listRaces) => {
                if (err) {
                    return;
                }
                if (onInit) {
                    client.emit('listRaces', listRaces);
                } else {
                    client.broadcast.emit('listRaces', listRaces);
                }
            }));
    }

    function getListTurns(zoneId) {
        Zone.findOne({
            _id: zoneId.zoneId
        }).exec(
            (err, zoneDB) => {
                if (err) {
                    return;
                }
                Subzone.aggregate([{
                    $match: {
                        zone: zoneDB._id
                    }
                }, {
                    $lookup: {
                        from: "turns",
                        localField: "_id",
                        foreignField: "subzone",
                        as: "turns"
                    }
                }, {
                    $unwind: {
                        path: "$turns",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $sort: {
                        "turns.created_at": -1
                    }
                }, {
                    $lookup: {
                        from: "drivers",
                        localField: "turns.driver",
                        foreignField: "_id",
                        as: "turns.driver",
                    }
                }, {
                    $group: {
                        _id: "$_id",
                        name: { $first: "$name" },
                        turns: { $addToSet: "$turns" }
                    }
                }]).exec(
                    (err, subzonesDB) => {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        client.emit('respListTurns', subzonesDB);

                    }
                );
            }
        )
    }

});