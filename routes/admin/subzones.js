const express = require('express');
const bcrypt = require('bcrypt');
const _ = require("underscore");
const Subzone = require('../../models/subzone');
const Zone = require('../../models/zone');
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication');
const { verifyZone } = require('../../middlewares/verifyparent');
const app = express();

app.get('/:zone_id/subzones', [verifyZone, verifyToken], function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }

    let zone_id = req.params.zone_id;

    Subzone.find({ zone: zone_id }, 'name coordinate zone status created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, subzones) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar bases',
                    errors: err
                });
            }
            Subzone.countDocuments({ zone: zone_id }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    zone: req.zone.name,
                    subzones
                })
            })
        }))
})

app.get('/:zone_id/subzones/:id', [verifyZone, verifyToken], function(req, res) {
    let id = req.params.id;
    Subzone.findById(id)
        .populate('zone')
        .exec((err, subzoneDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar base',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                subzone: subzoneDB
            })
        })
})

app.post('/:zone_id/subzones', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let subzone = new Subzone({
        name: body.name,
        coordinate: body.coordinate,
        zone: req.zone._id
    })
    subzone.save((err, subzoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar base',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            subzone: subzoneDB
        })
    });
})

app.put('/:zone_id/subzones/:id', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'coordinate', 'zone', 'status']);
    Subzone.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, subzoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar base',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            subzone: subzoneDB
        })
    })
})

app.delete('/:zone_id/subzones/:id', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let updateStatus = {
        status: false
    }
    Subzone.findByIdAndUpdate(id, updateStatus, (err, subzoneDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar base',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            subzone: subzoneDrop
        })
    })
})

module.exports = app;