const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Zone = require('../../models/zone')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()


app.get('/', verifyToken, function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }

    Zone.find({}, 'name coordinate polygon type_value service_value status created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, zones) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar zonas',
                    errors: err
                });
            }
            Zone.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    zones
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Zone.findById(id, (err, zoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar zona',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            zone: zoneDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let zone = new Zone({
        name: body.name,
        coordinate: body.coordinate,
        polygon: body.polygon,
        type_value: body.type_value,
        service_value: body.service_value
    })
    zone.save((err, zoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar zona',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            zone: zoneDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'coordinate', 'polygon', 'type_value', 'service_value', 'status']);
    Zone.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, zoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar zona',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            zone: zoneDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let updateStatus = {
        status: false
    }
    Zone.findByIdAndUpdate(id, updateStatus, (err, zoneDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar zona',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            zone: zoneDrop
        })
    })
})

module.exports = app;