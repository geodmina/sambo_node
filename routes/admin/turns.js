const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Turn = require('../../models/turn')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()


app.get('/', verifyToken, function(req, res) {
    Turn.find({}, 'subzone driver created_at updated_at')
        .sort({ created_at: 1 })
        .populate('subzone')
        .populate('driver')
        .exec(((err, turns) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar turnos',
                    errors: err
                });
            }
            Turn.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    turns
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Turn.findById(id, (err, turnDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar turno',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            turn: turnDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let turn = new Turn({
        subzone: body.subzone,
        driver: body.driver
    })
    turn.save((err, turnDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar turno',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            turn: turnDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['subzone', 'driver']);
    Turn.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, turnDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar turno',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            turn: turnDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    Turn.findByIdAndRemove(id, (err, turnDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar turno',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            turn: turnDrop
        })
    })
})

module.exports = app;