const express = require('express');
const _ = require("underscore");
const Call = require('../../models/call');
const Passenger = require('../../models/passenger');
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication');
const app = express();
const { io } = require('../../app');

app.get('/', verifyToken, function(req, res) {
    Call.find({}, 'extension phone operator attended_at created_at updated_at')
        .sort({ created_at: 1 })
        .populate('operator', 'name')
        .exec(((err, calls) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar llamadas',
                    errors: err
                });
            }
            Call.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    calls
                });
            });
        }));
});

app.get('/dispatcher', verifyToken, function(req, res) {
    var d = new Date();
    d.setMinutes(d.getMinutes() - 15);

    Call.find({ created_at: { $gte: d } }, 'extension phone operator attended_at created_at updated_at')
        .sort({ created_at: -1 })
        .populate('operator', 'name')
        .exec(((err, calls) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar llamadas',
                    errors: err
                });
            }

            Call.countDocuments({ created_at: { $gte: d } }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    calls
                });
            });
        }));
});

app.post('/dispatcher', [verifyToken], function(req, res) {
    let body = req.body;
    let call = new Call({
        extension: body.extension,
        phone: body.phone
    });
    call.save((err, callDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar llamada',
                errors: err
            });
        }

        getListRaces();

        res.status(201).json({
            ok: true,
            call: callDB
        });
    });

});

app.put('/dispatcher/:id', [verifyToken], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['extension', 'phone', 'operator', 'attended_at']);
    Call.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, callDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar llamada',
                errors: err
            });
        }

        getListRaces();

        Passenger.findOne({ phone: callDB.phone }, 'name email phone img').exec((err, passengerDB) => {
            if (err) {
                return res.status(200).json({
                    ok: true,
                    call: callDB,
                    passenger: {
                        ok: false
                    }
                });
            }
            if (!passengerDB) {
                return res.status(200).json({
                    ok: true,
                    call: callDB,
                    passenger: {
                        ok: false
                    }
                });
            }
            res.status(200).json({
                ok: true,
                call: callDB,
                passenger: {
                    ok: true,
                    passenger: passengerDB
                }
            });
        })

    });
});

function getListRaces() {
    var d = new Date();
    // d.setHours(d.getHours() - 1);
    d.setMinutes(d.getMinutes() - 15);
    Call.find({ created_at: { $gte: d } }, 'extension phone operator attended_at created_at updated_at')
        .sort({ created_at: -1 })
        .populate('operator', 'name')
        .exec(((err, calls) => {
            if (err) {
                return;
            }
            io.sockets.emit("listCalls", calls);
        }));
}

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Call.findById(id, (err, callDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar llamada',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            call: callDB
        });
    });
});

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let call = new Call({
        extension: body.extension,
        phone: body.phone
    });
    call.save((err, callDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar llamada',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            call: callDB
        });
    });
});

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['extension', 'phone', 'operator', 'attended_at']);
    Call.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, callDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar llamada',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            call: callDB
        });
    });
});

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    Call.findByIdAndRemove(id, (err, callDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar llamada',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            call: callDrop
        });
    });
});

module.exports = app;