const express = require('express');
const _ = require("underscore");
const Race = require('../../models/race');
const Status = require('../../models/status');
const { verifyToken, verifyAdmin, verifyAdminOperator } = require('../../middlewares/authentication');
const app = express();

app.get('/dispatcher', verifyToken, function(req, res) {
    var d = new Date();
    d.setMinutes(d.getMinutes() - 120);
    Race.find({ created_at: { $gte: d } })
        .sort({ created_at: -1 })
        .populate('passenger', 'name phone email img')
        .populate('driver', 'name phone numeral img')
        .populate('status', 'name')
        .populate('subzone', 'name')
        .populate({
            path: 'driver',
            populate: { path: 'car' }
        })
        .exec(((err, races) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar carreras',
                    errors: err
                });
            }
            Race.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    races
                });
            });
        }));
});


app.get('/', verifyToken, function(req, res) {
    let page = 0;
    let offset = 0;
    if (req.query.page && req.query.page > 0) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    Race.find({})
        .sort({ created_at: -1 })
        .skip(page * offset)
        .limit(offset)
        .populate('passenger', 'name phone email img')
        .populate('driver', 'name phone numeral img')
        .populate('status', 'name')
        .populate('subzone', 'name')
        .exec(((err, races) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar carreras',
                    errors: err
                });
            }
            Race.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    races
                });
            });
        }));
});

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Race.findById(id)
        .populate('passenger', 'name phone email img')
        .populate('driver', 'name phone numeral img')
        .populate('status', 'name')
        .populate('subzone', 'name')
        .exec((err, raceDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar carreras',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                race: raceDB
            });
        })
})


app.post('/', [verifyToken, verifyAdminOperator], function(req, res) {
    let body = req.body;

    Status.findOne({ name: 'INGRESADA' }, (err, statusDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar carrera',
                errors: err
            });
        }
        let race = new Race({
            address_origin: body.address_origin,
            lat_origin: body.lat_origin,
            lng_origin: body.lng_origin,
            reference: body.reference,
            passenger: body.passenger,
            payment: body.payment,
            status: statusDB._id,
            address_arrival: body.address_arrival,
            lat_arrival: body.lat_arrival,
            lng_arrival: body.lng_arrival,
            subzone: body.subzone,
            driver: body.driver,
            cost: body.cost,
            distance: body.distance,
            time_wait: body.time_wait,
            time_race: body.time_race,
            start_race: body.start_race,
            end_race: body.end_race,
            recalled: body.recalled,
            rate: body.rate,
            comment: body.comment,
            central: body.central
        })
        race.populate('passenger', 'name phone email img').execPopulate();
        race.populate('driver', 'name phone img').numeralexecPopulate();
        race.populate('status', 'name').execPopulate();
        race.populate('subzone', 'name').execPopulate();

        race.save((err, raceDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al registrar carrera',
                    errors: err
                });
            }
            res.status(201).json({
                ok: true,
                call: raceDB
            });
        });
    });
});



app.put('/:id', [verifyToken, verifyAdminOperator], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['address_origin', 'lat_origin', 'lng_origin', 'reference', 'passenger', 'payment', 'status', 'address_arrival', 'lat_arrival', 'lng_arrival', 'subzone', 'driver', 'cost', 'distance', 'time_wait', 'time_race', 'start_race', 'end_race', 'recalled', 'rate', 'comment']);

    Race.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' })
        .populate('passenger', 'name phone email img')
        .populate('driver', 'name phone numeral img')
        .populate('status', 'name')
        .populate('subzone', 'name')
        .exec((err, raceDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al actualizar carrera',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                race: raceDB
            });
        })


});

/* app.delete('/:id', [verifyToken, verifyAdminOperator], function(req, res) {
    let id = req.params.id;
    Race.findByIdAndRemove(id, (err, raceDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar carrera',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            call: raceDrop
        });
    });
}); */

module.exports = app;