var express = require('express');
var bcrypt = require('bcrypt');
var _ = require("underscore");

var { verifyToken, verifyAdmin } = require('../../middlewares/authentication');

var app = express();
var Passenger = require('../../models/passenger');

app.get('/search', verifyToken, function(req, res) {

    let page = 0;
    let offset = 0;
    if (req.query.page && req.query.page > 0) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    let status = req.query.status || true;
    let keyword = req.query.keyword.toUpperCase();
    const userRegex = new RegExp(keyword, 'i')
    Passenger.find({ status: status, $or: [{ name: userRegex }, { email: userRegex }, { phone: userRegex }] }, 'name email phone img fcmkey type administrator parent_id status created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .populate('parent_id', 'name email phone')
        .exec(((err, passengers) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error cargando usuarios pasajeros',
                    errors: err
                });
            }
            Passenger.countDocuments({ status: status, $or: [{ name: userRegex }, { email: userRegex }] }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    passengers
                })
            })
        }))
})

app.get('/', verifyToken, function(req, res) {

    let page = 0;
    let offset = 0;
    if (req.query.page && req.query.page > 0) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    let status = req.query.status || true;
    Passenger.find({ status: status }, 'name email phone img fcmkey type administrator parent_id status created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .populate('parent_id', 'name email phone')
        .exec(((err, passengers) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error cargando usuarios pasajeros',
                    errors: err
                });
            }
            Passenger.countDocuments({ status: status }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    passengers
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Passenger.findById(id)
        .populate('parent_id', 'name email phone')
        .exec((err, passengerDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error cargando usuario pasajero',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                passenger: passengerDB
            })
        })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let passenger = new Passenger({
        name: body.name,
        email: body.email,
        phone: body.phone,
        img: body.img,
        type: body.type,
        administrator: body.administrator,
        parent_id: body.parent_id,
        password: bcrypt.hashSync(body.password, 10)
    })
    passenger.save((err, passengerDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar usuario pasajero',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            passenger: passengerDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'phone', 'img', 'fcmkey', 'type', 'administrator', 'parent_id', 'status']);
    Passenger.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, passengerDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar usuario pasajero',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            passenger: passengerDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let updateStatus = {
        status: false
    }
    Passenger.findByIdAndUpdate(id, updateStatus, (err, passengerDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar usuario pasajero',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            passenger: passengerDrop
        })
    })
})

module.exports = app;