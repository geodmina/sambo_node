const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Driver = require('../../models/driver')
const StatusDriver = require('../../models/status_driver')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()

app.get('/search', verifyToken, function(req, res) {

    let page = 0;
    let jump = 0;
    if (req.query.page) {
        jump = Number(req.query.jump || 5);
        page = Number(req.query.page - 1);
    }
    let keyword = req.query.keyword.toUpperCase();
    let numeral = 99999999;
    if (!isNaN(keyword)) {
        numeral = parseInt(keyword);
    }
    const driverRegex = new RegExp(keyword, 'i')
    Driver.find({ $or: [{ name: driverRegex }, { email: driverRegex }, { numeral: numeral }] }, 'name email phone numeral img fcmkey status subzone position socketId created_at updated_at')
        .skip(page * jump)
        .limit(jump)
        .populate('status', 'name')
        .populate('car')
        .exec(((err, drivers) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar usuarios conductores',
                    errors: err
                });
            }
            Driver.countDocuments({ $or: [{ name: driverRegex }, { email: driverRegex }] }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    drivers
                })
            })
        }))
})

app.get('/', verifyToken, function(req, res) {

    let page = 0;
    let jump = 0;
    if (req.query.page) {
        jump = Number(req.query.jump || 5);
        page = Number(req.query.page - 1);
    }
    Driver.find({}, 'name email phone numeral img fcmkey status subzone position socketId created_at updated_at')
        .skip(page * jump)
        .limit(jump)
        .populate('status', 'name')
        .populate('car')
        .exec(((err, drivers) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar usuarios conductores',
                    errors: err
                });
            }
            Driver.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    drivers
                });
            });
        }));
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Driver.findById(id)
        .populate('status', 'name')
        .populate('car')
        .exec((err, driverDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar usuario conductor',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                driver: driverDB
            })
        })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;

    StatusDriver.findOne({ name: 'EN LINEA' }, (err, statusDriverDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar usuario conductor',
                errors: err
            });
        }
        let driver = new Driver({
            name: body.name,
            email: body.email,
            phone: body.phone,
            numeral: body.numeral,
            img: body.img,
            status: statusDriverDB._id,
            location: {
                type: "Point",
                coordinates: [0, 0]
            },
            password: bcrypt.hashSync(body.password, 10)
        })
        driver.save((err, driverDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al registrar usuario conductor',
                    errors: err
                });
            }
            res.status(201).json({
                ok: true,
                driver: driverDB
            });
        });
    });
});

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'phone', 'numeral', 'img', 'fcmkey', 'status', 'subzone', 'position', 'location']);
    Driver.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, driverDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualziar usuario conductor',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            driver: driverDB
        })
    });
});

/*app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let updateStatus = {
        status: false
    }
    Driver.findByIdAndUpdate(id, updateStatus, (err, DriverDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        if (!passengerDrop) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Passennger delete'
                }
            });
        }
        res.json({
            ok: true,
            driver: passengerDrop
        })
    })
})*/

module.exports = app;