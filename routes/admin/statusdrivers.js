const express = require('express')
const _ = require("underscore")
const StatusDriver = require('../../models/status_driver')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()

app.get('/', verifyToken, function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    StatusDriver.find({}, 'name created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, statusDrivers) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar estados de conductor',
                    errors: err
                });
            }
            StatusDriver.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    statusDrivers
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    StatusDriver.findById(id, (err, statusDriverDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consulktar estado de conductor',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            statusDriver: statusDriverDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let statusDriver = new StatusDriver({
        name: body.name
    })
    statusDriver.save((err, statusDriverDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registra estado de conductor',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            statusDriver: statusDriverDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name']);
    console.log(body)
    StatusDriver.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, statusDriverDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar estado de conductor',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            statusDriver: statusDriverDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    StatusDriver.findByIdAndRemove(id, (err, statusDriverDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar estado de conductor',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            statusDriver: statusDriverDrop
        })
    })
})

module.exports = app;