const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Direction = require('../../models/direction')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()

app.get('/search', verifyToken, function(req, res) {

    var keyword = req.query.keyword.toUpperCase();
    const directionRegex = new RegExp(keyword, 'i');
    Direction.find({ address: directionRegex }, 'address latitude longitude created_at updated_at')
        .exec(((err, directions) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar direcciones',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                directions
            })
        }))
})

app.get('/', verifyToken, function(req, res) {
    let page = 0;
    let jump = 0;
    if (req.query.page) {
        jump = Number(req.query.jump || 5);
        page = Number(req.query.page - 1);
    }
    Direction.find({}, 'address latitude longitude created_at updated_at')
        .skip(page * jump)
        .limit(jump)
        .exec(((err, directions) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar direcciones',
                    errors: err
                });
            }
            Direction.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    directions
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Direction.findById(id, (err, directionDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar dirección',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            direction: directionDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let direction = new Direction({
        address: body.address.toUpperCase(),
        latitude: body.latitude,
        longitude: body.longitude
    })
    direction.save((err, directionDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar dirección',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            direction: directionDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['address', 'latitude', 'longitude']);
    Direction.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, directionDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar dirección',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            direction: directionDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    Direction.findByIdAndRemove(id, (err, directionDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al eliminar dirección',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            direction: directionDrop
        })
    })
})

module.exports = app;