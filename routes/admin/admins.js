var express = require('express');
var bcrypt = require('bcrypt');
var _ = require("underscore");

var { verifyToken, verifyAdmin } = require('../../middlewares/authentication');

var app = express();
var Admin = require('../../models/admin');

app.get('/', verifyToken, function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    var status = req.query.status || true;
    Admin.find({ status: status }, 'name username email role status img created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, admins) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error cargando usuarios admins',
                    errors: err
                });
            }
            Admin.countDocuments({ status: status }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    admins
                })
            })
        }))

});

app.get('/:id', verifyToken, function(req, res) {
    var id = req.params.id;
    Admin.findById(id, (err, adminDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error cargando usuarios admins',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            admin: adminDB
        })
    })
});

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    var body = req.body;
    var admin = new Admin({
        name: body.name,
        username: body.username,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    })
    admin.save((err, adminDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar usuario admin',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            admin: adminDB
        })
    })
});

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    console.log(req.body)
    var id = req.params.id;
    var body = _.pick(req.body, ['name', 'username', 'email', 'img', 'role', 'status']);
    Admin.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, adminDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar usuario admin',
                errors: err
            });
        }
        res.json({
            ok: true,
            admin: adminDB
        })
    })
});

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    var id = req.params.id;
    var updateStatus = {
        status: false
    };
    Admin.findByIdAndUpdate(id, updateStatus, (err, adminDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar usuario admin',
                errors: err
            });
        }
        if (!adminDrop) {
            return res.status(200).json({
                ok: false,
                message: 'Usuario admin borrado',
                errors: err
            });
        }
        res.json({
            ok: true,
            admin: adminDrop
        })
    });
});

module.exports = app;