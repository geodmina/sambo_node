const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Parameter = require('../../models/parameter')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()


app.get('/', verifyToken, function(req, res) {

    let page = 0;
    let jump = 0;
    if (req.query.page) {
        jump = Number(req.query.jump || 5);
        page = Number(req.query.page - 1);
    }

    Parameter.find({}, 'name code text_value numeric_value created_at updated_at')
        .skip(page * jump)
        .limit(jump)
        .exec(((err, parameters) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar parametros',
                    errors: err
                });
            }
            Parameter.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    parameters
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Parameter.findById(id, (err, parameterDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar parametro',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            parameter: parameterDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let parameter = new Parameter({
        name: body.name,
        code: body.code,
        text_value: body.text_value,
        numeric_value: body.numeric_value
    })
    parameter.save((err, parameterDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar parametro',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            parameter: parameterDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'code', 'text_value', 'numeric_value']);
    Parameter.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, parameterDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar parametro',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            parameter: parameterDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    Parameter.findByIdAndRemove(id, (err, parameterDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar parametro',
                errors: err
            });
        }
        res.json({
            ok: true,
            parameter: parameterDrop
        })
    })
})

module.exports = app;