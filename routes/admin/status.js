const express = require('express')
const _ = require("underscore")
const Status = require('../../models/status')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const app = express()

app.get('/', verifyToken, function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }
    Status.find({}, 'name created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, status) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar estados',
                    errors: err
                });
            }
            Status.countDocuments({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    status
                })
            })
        }))
})

app.get('/:id', verifyToken, function(req, res) {
    let id = req.params.id;
    Status.findById(id, (err, statusDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al consultar estado',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            status: statusDB
        })
    })
})

app.post('/', [verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let status = new Status({
        name: body.name
    })
    status.save((err, statusDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar estado',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            status: statusDB
        })
    });
})

app.put('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name']);
    console.log(body)
    Status.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, statusDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualziar estado',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            status: statusDB
        })
    })
})

app.delete('/:id', [verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    Status.findByIdAndRemove(id, (err, statusDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar estado',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            user: statusDrop
        })
    })
})

module.exports = app;