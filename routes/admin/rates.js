const express = require('express')
const bcrypt = require('bcrypt')
const _ = require("underscore")
const Rate = require('../../models/rate')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication')
const { verifyZone } = require('../../middlewares/verifyparent')
const app = express()

app.get('/:zone_id/rates', [verifyZone, verifyToken], function(req, res) {

    var page = 0;
    var offset = 0;
    if (req.query.page) {
        offset = Number(req.query.offset || 5);
        page = Number(req.query.page - 1);
    }

    let zone_id = req.params.zone_id;

    Rate.find({ zone: zone_id }, 'name zone value_start cost_min time_value distance_value time_start time_end time_wait status created_at updated_at')
        .skip(page * offset)
        .limit(offset)
        .exec(((err, rates) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al listar tarifas',
                    errors: err
                });
            }
            Rate.countDocuments({ zone: zone_id }, (err, count) => {
                res.status(200).json({
                    ok: true,
                    count: count,
                    zone: req.zone.name,
                    rates
                })
            })
        }))
})

app.get('/:zone_id/rates/:id', [verifyZone, verifyToken], function(req, res) {
    let id = req.params.id;
    Rate.findById(id)
        .populate('zone')
        .exec((err, rateDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar tarifa',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                rate: rateDB
            })
        })
})

app.post('/:zone_id/rates', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let body = req.body;
    let rate = new Rate({
        name: body.name,
        zone: req.zone._id,
        value_start: body.value_start,
        cost_min: body.cost_min,
        time_value: body.time_value,
        distance_value: body.distance_value,
        time_start: body.time_start,
        time_end: body.time_end,
        time_wait: body.time_wait

    })
    rate.save((err, rateDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar tarifa',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            rate: rateDB
        })
    });
})

app.put('/:zone_id/rates/:id', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'zone', 'value_start', 'cost_min', 'time_value', 'distance_value', 'time_start', 'time_end', 'time_wait', 'status']);
    Rate.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, rateDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al actualizar tarifa',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            rate: rateDB
        })
    })
})


app.delete('/:zone_id/rates/:id', [verifyZone, verifyToken, verifyAdmin], function(req, res) {
    let id = req.params.id;
    let updateStatus = {
        status: false
    }
    Rate.findByIdAndUpdate(id, updateStatus, (err, rateDrop) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al borrar tarifa',
                errors: err
            });
        }
        res.status(200).json({
            ok: true,
            rate: rateDrop
        })
    })
})


module.exports = app;