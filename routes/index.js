var express = require('express');
var app = express();

// Importar rutas

// Rutas de matenimientos de admin
var adminRoutes = require('./admin/admins');
var userRoutes = require('./admin/passengers');
var statusRoutes = require('./admin/status');
var statusdriversRoutes = require('./admin/statusdrivers');
var parametersRoutes = require('./admin/parameters');
var directionsRoutes = require('./admin/directions');
var zonesRoutes = require('./admin/zones');
var subzonesRoutes = require('./admin/subzones');
var callsRoutes = require('./admin/calls');
var driversRoutes = require('./admin/drivers');
var ratesRoutes = require('./admin/rates');
var turnsRoutes = require('./admin/turns');
var menuRoutes = require('./menu/menu');
var raceRoutes = require('./admin/races');

// Rutas de login
var adminLoginRoutes = require('./login/adminlogin');
var passengerLoginRoutes = require('./login/passengerlogin');
var driverLoginRoutes = require('./login/driverlogin');

// Rutas de servicios de driver
var driverServicesRoutes = require('./driver/services');

// Rutas en comun
var uploadRoutes = require('./upload');
var imageRoutes = require('./images');

// Rutas
app.use('/admins', adminLoginRoutes);
app.use('/passengers', passengerLoginRoutes);
app.use('/drivers', driverLoginRoutes)

app.use('/admin/admins', adminRoutes);
app.use('/admin/passengers', userRoutes);
app.use('/admin/status', statusRoutes);
app.use('/admin/status_drivers', statusdriversRoutes);
app.use('/admin/parameters', parametersRoutes);
app.use('/admin/directions', directionsRoutes);
app.use('/admin/zones', zonesRoutes);
app.use('/admin/zones', subzonesRoutes);
app.use('/admin/calls', callsRoutes);
app.use('/admin/drivers', driversRoutes);
app.use('/admin/zones', ratesRoutes);
app.use('/admin/turns', turnsRoutes);
app.use('/admin/races', raceRoutes);
app.use('/admin/menu', menuRoutes);

app.use('/driver/services', driverServicesRoutes);

app.use('/upload', uploadRoutes);
app.use('/img', imageRoutes);

module.exports = app;