const express = require('express');
const bcrypt = require('bcrypt');
const _ = require("underscore");

const Zone = require('../../models/zone');
const Subzone = require('../../models/subzone');
const Turn = require('../../models/turn');
const Rate = require('../../models/rate');
const Race = require('../../models/race');
const Driver = require('../../models/driver');
const Status = require('../../models/status');

const Car = require('../../models/car');

const app = express();

/* app.get('/car', function(req, res) {

    Car.find({})
        .exec(
            (err, ok) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al consultar connductor',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    message: 'ok',
                    driver: ok
                });
            }
        )

}); */

app.get('/timerace', function(req, res) {

    Status.findOne({ name: 'SIN RESPUESTA' })
        .exec((err, statusDB) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar tiempo unidad',
                    errors: err
                });
            }

            var d = new Date();
            d.setMinutes(d.getMinutes() - 1500);
            Race.find({ created_at: { $gte: d }, status: statusDB })
                .sort({ created_at: -1 })
                .populate('passenger', 'name phone')
                .populate('driver', 'name numeral')
                .populate('status', 'name')
                .populate({
                    path: 'driver',
                    populate: { path: 'car' }
                })
                .exec(((err, listRaces) => {
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            message: 'Error al consultar tiempo unidad',
                            errors: err
                        });
                    }

                    if (listRaces.length < 1) {
                        return res.status(204).json({
                            ok: false,
                            message: 'No hay datos'
                        });
                    }

                    res.status(200).json({
                        ok: true,
                        message: 'ok',
                        races: listRaces
                    });

                }));

        });

});

app.get('/driver', function(req, res) {

    if (req.query._id) {

        Driver.findOne({ _id: req.query._id })
            .populate('status', 'name')
            .populate('subzone', 'name')
            .populate('car')
            .exec(
                (err, driverDB) => {
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            message: 'Error al consultar connductor',
                            errors: err
                        });
                    }

                    if (!driverDB) {
                        return res.status(204).json({
                            ok: false,
                            message: 'No hay datos'
                        });
                    }

                    res.status(200).json({
                        ok: true,
                        message: 'ok',
                        driver: driverDB
                    });

                }
            )

    } else {
        return res.status(400).json({
            ok: false,
            message: 'Datos incompletos'
        });
    }

});

app.get('/history', function(req, res) {

    if (req.query.month && req.query.year && req.query._id) {

        var date = new Date();
        date.setFullYear(req.query.year);
        date.setMonth(req.query.month - 1);

        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1, 19, 0, 0);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0, 18, 59, 59);

        Race.find({
                driver: req.query._id,
                $and: [{ created_at: { $gte: firstDay } }, { created_at: { $lte: lastDay } }]
            })
            .sort({ created_at: -1 })
            .populate('passenger', 'name phone email img')
            .populate({
                path: 'driver',
                populate: { path: 'status' }
            })
            .populate({
                path: 'driver',
                populate: { path: 'subzone' }
            })
            .populate({
                path: 'driver',
                populate: { path: 'car' }
            })
            .populate('status', 'name')
            .populate('subzone', 'name')
            .populate('fare')
            .exec(
                (err, raceDB) => {
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            message: 'Error al consultar historial',
                            errors: err
                        });
                    }

                    if (raceDB.length < 1) {
                        return res.status(204).json({
                            ok: false,
                            message: 'No hay datos'
                        });
                    }

                    res.status(200).json({
                        ok: true,
                        message: 'ok',
                        races: raceDB
                    });

                }
            )

    } else {
        return res.status(400).json({
            ok: false,
            message: 'Datos incompletos'
        });
    }

});

app.get('/rates', function(req, res) {

    if (req.body.hour && req.body.minutes && req.body.latitude && req.body.longitude) {
        const dateSearch = new Date(2000, 0, 1, req.body.hour, req.body.minutes, 0);
        dateSearch.setUTCDate(1);

        Rate.findOne({
            $or: [{
                    $and: [{ time_start: { $lte: dateSearch } }, { time_end: { $gte: dateSearch } }]
                },
                {
                    $and: [{ time_start: { $lte: dateSearch } }, { $expr: { $gte: ["$time_start", "$time_end"] } }]
                },
                {
                    $and: [{ time_end: { $gte: dateSearch } }, { $expr: { $gte: ["$time_start", "$time_end"] } }]
                }
            ]
        }).exec((err, rateDB) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar tarifas',
                    errors: err
                });
            }

            if (rateDB) {
                res.status(200).json({
                    ok: true,
                    message: 'ok',
                    rate: rateDB
                });
            } else {
                return res.status(204).json({
                    ok: false,
                    message: 'Error al consultar tarifas',
                    errors: err
                });
            }

        });

    } else {
        return res.status(400).json({
            ok: false,
            message: 'Datos incompletos'
        });
    }
});

app.get('/turns', function(req, res) {

    if (req.query.latitude && req.query.longitude) {
        let coordinates = [req.query.latitude, req.query.longitude];
        Zone.find({
            polygon: {
                $geoIntersects: {
                    $geometry: coordinates
                }
            }
        }).find(
            (err, zoneDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al listar turnos',
                        errors: err
                    });
                }
                if (zoneDB.length > 0) {

                    Subzone.aggregate([
                        { $match: { zone: zoneDB[0]._id } },
                        {
                            $lookup: {
                                from: "turns",
                                localField: "_id",
                                foreignField: "subzone",
                                as: "turns"
                            }
                        }
                    ]).exec(
                        (err, subzonesDB) => {
                            if (err) {
                                return res.status(400).json({
                                    ok: false,
                                    message: 'Error al listar bases',
                                    errors: err
                                });
                            }

                            turns = [];

                            subzonesDB.forEach(function(subzone) {
                                turns.push({
                                    subzone: subzone.name,
                                    count: subzone.turns.length
                                });
                            });

                            res.status(200).json({
                                ok: true,
                                message: 'ok',
                                turns: turns
                            });

                        }
                    );
                } else {
                    return res.status(204).json({
                        ok: false,
                        message: 'No se encuentra en una zona'
                    });
                }

            }
        );

    } else {
        return res.status(400).json({
            ok: false,
            message: 'No ha enviado datos de ubicación'
        });
    }

});

module.exports = app;