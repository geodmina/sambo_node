var express = require('express');
var bcrypt = require('bcrypt');
var _ = require("underscore");
var jwt = require('jsonwebtoken');

var SEED = require('../../config/config').SEED;
var CADUCIDAD_TOKEN = require('../../config/config').CADUCIDAD_TOKEN;

var { verifyTokenPassenger } = require('../../middlewares/authentication');

var app = express();
var Passenger = require('../../models/passenger');

app.post('/sign_in', (req, res) => {
    var body = req.body;
    Passenger.findOne({ $or: [{ email: body.email }, { phone: body.email }] }, (err, passengerDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error cargando usuario pasajeros',
                errors: err
            });
        }
        if (!passengerDB) {
            return res.status(401).json({
                ok: false,
                message: 'Usuario o contraseña incorrectos',
                errors: err
            });
        }
        if (!bcrypt.compareSync(body.password, passengerDB.password)) {
            return res.status(401).json({
                ok: false,
                message: 'Usuario o contraseña incorrectos',
                errors: err
            });
        }

        var token = jwt.sign({
            passenger: passengerDB
        }, SEED, { expiresIn: CADUCIDAD_TOKEN });

        res.status(200).json({
            ok: true,
            passenger: passengerDB,
            token: token,
            id: passengerDB._id
        });
    });
});

app.post('/sign_up', function(req, res) {
    var body = req.body;
    var passenger = new Passenger({
        name: body.name,
        phone: body.phone,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10)
    });
    passenger.save((err, passengerDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar usuario pasajero',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            passenger: passengerDB
        });
    });
});

app.put('/profile/:id', verifyTokenPassenger, function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'phone', 'img', 'fcmkey', 'type', 'administrator', 'parent_id', 'status']);
    Passenger.findByIdAndUpdate(id, body, { new: true, runValidators: true, context: 'query' }, (err, passengerDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!passengerDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            passenger: passengerDB
        })
    })
})

module.exports = app;