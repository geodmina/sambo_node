var express = require('express');
var bcrypt = require('bcrypt');
var _ = require("underscore");
var jwt = require('jsonwebtoken');

var SEED = require('../../config/config').SEED;
var CADUCIDAD_TOKEN = require('../../config/config').CADUCIDAD_TOKEN;

var app = express();
var Driver = require('../../models/driver');

app.post('/sign_in', (req, res) => {
    var body = req.body;
    console.log(body);
    Driver.findOne({
            $or: [
                { email: body.email }, { phone: body.email }, { imei: body.email }
            ]
        })
        .populate('status', 'name')
        .populate('subzone', 'name')
        .populate('car')
        .exec(function(err, driverDB) {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error cargando usuario conductor',
                    errors: err
                });
            }
            if (!driverDB) {
                return res.status(401).json({
                    ok: false,
                    message: 'Usuario o contraseña incorrectos',
                    errors: err
                });
            }
            if (!bcrypt.compareSync(body.password, driverDB.password)) {
                return res.status(401).json({
                    ok: false,
                    message: 'Usuario o contraseña incorrectos',
                    errors: err
                });
            }

            var token = jwt.sign({
                driver: driverDB
            }, SEED, { expiresIn: CADUCIDAD_TOKEN });


            res.status(200).json({
                ok: true,
                driver: driverDB,
                token: token,
                id: driverDB._id
            });
        });
});

module.exports = app;