var express = require('express');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

var SEED = require('../../config/config').SEED;
var CADUCIDAD_TOKEN = require('../../config/config').CADUCIDAD_TOKEN;

var app = express();
var Admin = require('../../models/admin');
var Menu = require('../../models/menu');

app.post('/sign_in', (req, res) => {
    var body = req.body;
    Admin.findOne({ $or: [{ email: body.email }, { username: body.email }] }, (err, adminDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error cargando usuarios admins',
                errors: err
            });
        }
        if (!adminDB) {
            return res.status(401).json({
                ok: false,
                message: 'Usuario o contraseña incorrectos',
                errors: err
            });
        }
        if (!bcrypt.compareSync(body.password, adminDB.password)) {
            return res.status(401).json({
                ok: false,
                message: 'Usuario o contraseña incorrectos',
                errors: err
            });
        }

        var token = jwt.sign({
            admin: adminDB
        }, SEED, { expiresIn: CADUCIDAD_TOKEN });

        res.status(200).json({
            ok: true,
            admin: adminDB,
            token: token,
            id: adminDB._id
        });
    });
});

app.post('/sign_up', function(req, res) {
    var body = req.body;
    var admin = new Admin({
        name: body.name,
        username: body.username,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10)
    });
    admin.save((err, adminDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'Error al registrar usuario admin',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            admin: adminDB
        });
    });
});

module.exports = app;