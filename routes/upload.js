var express = require('express');
var fileUpload = require('express-fileupload');
var fs = require('fs');

var app = express();
var Admin = require('../models/admin');
var Passenger = require('../models/passenger');
var Driver = require('../models/driver');

app.use(fileUpload());

app.put('/:resource/:id', function(req, res, next) {
    var resource = req.params.resource;
    var id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No seleccionó nada.',
            errors: {
                message: 'Debe selecionar una imagen'
            }
        });
    }

    // Obtener nombre del archivo
    var file = req.files.imagen;
    var nameShort = file.name.split('.');
    var extensionFile = nameShort[nameShort.length - 1];

    // Sólo estas extensiones aceptamos
    var extensionValid = ['png', 'jpg', 'gif', 'jpeg'];

    if (extensionValid.indexOf(extensionFile.toLowerCase()) < 0) {
        return res.status(400).json({
            ok: false,
            message: 'Extension no válida',
            errors: {
                message: 'Las extensiones válidas son ' + extensionValid.join(', ')
            }
        });
    }

    // Nombre de archivo personalizado
    var nameFile = `${ id }-${ new Date().getMilliseconds() }.${ extensionFile }`;

    // Mover el archivo del temporal a un path
    var path = `./uploads/${ resource  }/${ nameFile }`;

    file.mv(path, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al mover archivo',
                errors: err
            });
        }

        uploadImageResource(resource, id, nameFile, res);

    })
});

function uploadImageResource(resource, id, nameFile, res) {
    if (resource === 'admins') {
        Admin.findById(id, (err, admin) => {
            if (!admin) {
                return res.status(400).json({
                    ok: false,
                    message: 'Usuario admin no existe',
                    errors: {
                        message: 'Usuario admin no existe'
                    }
                });
            }

            var pathOld = './uploads/admins/' + admin.img;

            // Si existe, elimina la imagen anterior
            if (fs.existsSync(pathOld)) {
                fs.unlink(pathOld, (err) => {})
            }

            admin.img = nameFile;

            admin.save((err, adminDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al actualizar usuario admin',
                        errors: err
                    });
                }
                return res.status(200).json({
                    ok: true,
                    message: 'Imagen de usuario admin actualizada',
                    admin: adminDB
                });
            })
        });
    }

    if (resource === 'passengers') {
        Passenger.findById(id, (err, passenger) => {
            if (!passenger) {
                return res.status(400).json({
                    ok: false,
                    message: 'Usuario pasajero no existe',
                    errors: {
                        message: 'Usuario pasajero no existe'
                    }
                });
            }

            var pathOld = './uploads/passengers/' + passenger.img;

            // Si existe, elimina la imagen anterior
            if (fs.existsSync(pathOld)) {
                fs.unlink(pathOld, (err) => {})
            }

            passenger.img = nameFile;

            passenger.save((err, passengerDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al actualizar usuario pasajero',
                        errors: err
                    });
                }
                return res.status(200).json({
                    ok: true,
                    message: 'Imagen de usuario pasajero actualizada',
                    passenger: passengerDB
                });
            })
        });
    }

    if (resource === 'drivers') {
        Driver.findById(id, (err, driver) => {
            if (!driver) {
                return res.status(400).json({
                    ok: false,
                    message: 'Usuario conductor no existe',
                    errors: {
                        message: 'Usuario conductor no existe'
                    }
                });
            }

            var pathOld = './uploads/drivers/' + driver.img;

            // Si existe, elimina la imagen anterior
            if (fs.existsSync(pathOld)) {
                fs.unlink(pathOld, (err) => {})
            }

            driver.img = nameFile;

            driver.save((err, driverDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al actualizar usuario conductor',
                        errors: err
                    });
                }
                return res.status(200).json({
                    ok: true,
                    message: 'Imagen de usuario conductor actualizada',
                    driver: driverDB
                });
            })
        });
    }

}

module.exports = app;