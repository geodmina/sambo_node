const express = require('express');
const Menu = require('../../models/menu')
const { verifyToken, verifyAdmin } = require('../../middlewares/authentication');
const app = express();

app.get('/:role', function(req, res) {

    let role = req.params.role;

    Menu.findOne({ role: role })
        .exec(((err, menu) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al consultar menu',
                    errors: err
                });
            }
            if (!menu) {
                return res.status(400).json({
                    ok: false,
                    message: 'No hay un menu configurado para el rol ingresado',
                    errors: err
                });
            }
            res.status(200).json({
                ok: true,
                menu
            })
        }))
})


module.exports = app;