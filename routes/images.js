var express = require('express');

var app = express();

var path = require('path');
var fs = require('fs');

app.get('/:resource/:img', (req, res, next) => {
    var resource = req.params.resource;
    var img = req.params.img;
    var pathImg = path.resolve(__dirname, `../uploads/${ resource }/${ img }`);

    if (fs.existsSync(pathImg)) {
        res.sendFile(pathImg);
    } else {
        var pathNoImg = path.resolve(__dirname, '../assets/no-img.jpg');
        res.sendFile(pathNoImg);
    }

});

module.exports = app;