// =============================================
// Verificar Zone
// =============================================
const Zone = require('../models/zone')

let verifyZone = (req, res, next) => {
    let zone_id = req.params.zone_id;
    Zone.findById(zone_id, (err, zoneDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                message: 'La Zona no es correcta',
                errors: err
            });
        }
        req.zone = zoneDB;
        next();
    })
}

module.exports = {
    verifyZone
}