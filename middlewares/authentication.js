var jwt = require('jsonwebtoken')

var SEED = require('../config/config').SEED;

// =============================================
// Verificar TOKEN
// =============================================
var verifyToken = (req, res, next) => {
    var token = req.get('Authorization');
    jwt.verify(token, SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                message: "Token no valido",
                errors: err
            })
        }
        req.admin = decoded.admin;
        next();
    })
};

var verifyAdmin = (req, res, next) => {
    var admin = req.admin;
    if (admin.role === 'ADMIN_ROLE') {
        next();
    } else {
        return res.status(401).json({
            ok: false,
            mensaje: 'Token incorrecto - No es administrador',
            errors: {
                message: 'No es administrador, acción no permitida'
            }
        })
    }
};

var verifyAdminOperator = (req, res, next) => {
    var admin = req.admin;
    if (admin.role === 'ADMIN_ROLE' || admin.role === 'OPERADOR_ROLE') {
        next();
    } else {
        return res.status(401).json({
            ok: false,
            mensaje: 'Token incorrecto - No es administrador ni operador',
            errors: {
                message: 'No es administrador ni operador, acción no permitida'
            }
        })
    }
};


var verifyTokenPassenger = (req, res, next) => {
    var token = req.get('Authorization');
    jwt.verify(token, SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                message: "Token no valido",
                errors: err
            })
        }
        req.passenger = decoded.passenger;
        next();
    })
};

module.exports = { verifyToken, verifyAdmin, verifyAdminOperator, verifyTokenPassenger };